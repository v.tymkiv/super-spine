import { Component, gsap } from "engine";
import * as styles from "./info-item.module.css";

export default class extends Component {
    constructor({ text }) {
        super();

        this._DOM = this._htmlToElement(`<li class="${ styles["info-item"] }"> <span class="${ styles["info-item__text"] }"> ${ text } </span> </li>`);
    }

    show() {
        this._tween?.kill();
        if (Number(this._DOM.style.opacity) === 1) return;

        this._tween = gsap.to(this._DOM, { opacity: 1, duration: 0.3 });
        return this._tween.then();
    }

    hide() {
        this._tween?.kill();
        if (Number(this._DOM.style.opacity) === 0) return;

        this._tween = gsap.to(this._DOM, { opacity: 0, duration: 0.3 });
        return this._tween.then();
    }
}