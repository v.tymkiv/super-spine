export default class Component {
    constructor() {
        this._DOM = null;
    }

    /**
     * Возвращает DOM элемент компонента
     * @returns {Element} - DOM элемент
     */
    get DOM() {
        return this._DOM;
    }

    /**
     * Создаёт DOM элемент из строки
     * @param {String} html - должен содержать один родительский элемент
     * @return {Element} - DOM элемент
     */
    _htmlToElement(html) {
        const template = document.createElement("template");
        template.innerHTML = html;
        return template.content.firstChild;
    }

    /**
     * Удаляет DOM элемент из DOM
     */
    destroy() {
        this._DOM.remove();
        this._DOM = null;
    }
}