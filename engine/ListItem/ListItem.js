import { Component, gsap } from "engine";
import * as styles from "./list-item.module.css";

export default class ListItem extends Component {
    constructor({ name, showInstantly, onClickRemove, ...otherProps } = {}) {
        super();

        this.name = name;

        this._onClickRemove = onClickRemove;
        this._showInstantly = showInstantly;

        this._createDOM({...otherProps});

        if (!this._showInstantly) {
            gsap.set(this._wrapper, { yPercent: -100 });
            gsap.set(this._DOM, { height: 0 });
        }

        this._addListeners();
    }

    _createDOM() {
        this._DOM = this._htmlToElement(`<li class="${ styles["item"] }"></li>`);
        this._wrapper = this._htmlToElement(`<div class="${ styles["item__wrapper"] }"></div>`);
        this._label = this._htmlToElement(`<label class="${ styles["item__label"] }"></label>`);
        this._title = this._htmlToElement(`<span class="${ styles["item__title"] }">${ this.name }</span>`);
        this._removeBtn = this._htmlToElement(`<button type="button" class="${ styles["item__remove-btn"] }"></button>`);

        this._DOM.append(this._wrapper);
        this._wrapper.append(this._label);
        this._wrapper.append(this._removeBtn);
        this._label.append(this._title);
    }

    add() {
        this._timeline?.kill();
        this._timeline = gsap.timeline()
            .to(this._wrapper, { yPercent: 0, duration: 0.3 }, 0)
            .to(this._DOM, { height: 40, duration: 0.3 }, 0);

        return this._timeline.then();
    }

    remove() {
        this._timeline?.kill();
        this._timeline = gsap.timeline()
            .to(this._wrapper, { yPercent: -100, duration: 0.3 }, 0)
            .to(this._DOM, { height: 0, duration: 0.3 }, 0);

        return this._timeline.then();
    }

    _addListeners() {
        this._removeBtn.addEventListener("click", () => this._onClickRemove(this));
    }
}