import gsap from "gsap";
import * as PIXI from "pixi.js";
import "pixi-spine";

import Storage from "./Storage";
import Component from "./Component";
import List from "./List";
import InfoItem from "./InfoItem";
import ListItem from "./ListItem";


const storage = new Storage();

window.storage = storage;

export { Component, storage, gsap, PIXI, List, ListItem, InfoItem };