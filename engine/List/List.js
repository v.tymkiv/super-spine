import { gsap, Component, storage, InfoItem as InfoItemDefault, ListItem as ListItemDefault } from "engine";
import * as styles from "./list.module.css";
import DragAndDropListItems from "./DragAndDrop";

export default class List extends Component {
    constructor({ ListItem, InfoItem, storageKEY, listItemProps, infoItemProps } = {}) {
        super();

        this.storageKEY = storageKEY;
        this._ListItem = ListItem || ListItemDefault;
        this._InfoItem = InfoItem || InfoItemDefault;
        this._listItemProps = listItemProps;

        this._DOM = this._htmlToElement(`<ul class="${styles["list"]}"></ul>`);
        this._resources = [];

        this._infoItemComponent = new this._InfoItem(infoItemProps);
        this._infoItem = this._infoItemComponent.DOM;

        this._dragAndDropListItemsManager = new DragAndDropListItems(this._DOM, (listItemComponents) => this._onDragAndDropSort(listItemComponents))

        this._listeners = {};

        storage.addListener(this.storageKEY, (outsideResources) => this._update(outsideResources));
    }

    /**
     * Обновление данных. Удаляет лишние ресурсы, создает и добавляет новые
     * @param {{id: String, name: String}[]} outsideResources - массив ресурсов
     * @param {Boolean} showInstantly - показать мгновенно (без анимации появления)
     * @returns {Promise<void>}
     * @private
     */
    async _update(outsideResources = [], showInstantly = false) {
        // Если переданный массив resources содержал повторяющиеся элементы
        const uniqueOutsideResources = outsideResources.reduce((uniqueOutsideResources, outsideResource) => uniqueOutsideResources.map(({ id }) => id).includes(outsideResource.id) ? uniqueOutsideResources : [...uniqueOutsideResources, outsideResource], [])
        if (uniqueOutsideResources.length !== outsideResources.length) {

            // set массива из уникальных компонентов
            storage.set(this.storageKEY, uniqueOutsideResources);

            // set дёрнет метод update опять, по этому делаем return
            return;
        }

        // Удаление лишних ресурсов. Создание массива listItemComponent, которых нужно будет удалить
        const listItemComponentsToDelete = this._deleteUselessResources(outsideResources);
        this._dragAndDropListItemsManager.removeComponents(listItemComponentsToDelete);
        this.onAfterRemove();

        // Копирую внутренности outsideResource в this._resource
        // Если этого не сделать, то рассинхронизация может привести к багу
        this._resources = this._resources.map(resource => ({
            ...resource,
            ...outsideResources.find(({id}) => id === resource.id)
        }));

        // Добавление новых resource ячеек
        const newResourceItems = this._createNewResourceItems(outsideResources);
        this._resources.push(...newResourceItems);
        this.onAfterAdd(this.clone(newResourceItems));

        // Все анимация должны быть вызваны только после изменения this_resources
        // Иначе, при одновременном срабатывании сразу нескольких update - будут ошибочно созданы или удалены лишние элементы

        // Анимация удаления
        await this._animationRemoving(listItemComponentsToDelete);
        this.onAfterRemoveAnimation();

        // Анимация скрытия сообщения об пустом списке
        if (this._resources.length !== 0) {
            await this._infoItemComponent.hide();
            this._infoItem.remove();
        }

        // Создание новых listItemComponent, ожидание окончания анимаций появления
        const listItemComponents = this._createListItems(showInstantly);
        this._dragAndDropListItemsManager.addComponents(listItemComponents);
        await Promise.all(listItemComponents.map(listItemComponent => this._appendListItems(listItemComponent, showInstantly)));
        this.onAfterAddAnimation();

        // Анимация появления сообщения об пустом списке
        if (this._resources.length === 0) {
            this._DOM.append(this._infoItem);
            this._infoItemComponent.show();
        }

        if (this._isNeededSorting(outsideResources)){
            // Анимация сортировки
            await this._animateSortBy(outsideResources);

            this._sortResourcesBy(outsideResources);

            this._reAppend();
        }
    }

    _isNeededSorting(outsideResources) {
        return this._resources.reduce((isNeed, resource, index) => resource.id === outsideResources[index].id ? isNeed : true , false);
    }

    _animateSortBy(outsideResources) {
        return Promise.all(this._resources
            .reduce((promiseArray, resource, index) => {
                const outsideResourceIndex = outsideResources.findIndex(({ id }) => id === resource.id);
                const number = outsideResourceIndex - index;
                return number ? [...promiseArray, this._translateItem(resource.listItemComponent, number)] : promiseArray;
            }, []));
    }

    _sortResourcesBy(outsideResources) {
        // Сортировка внутри this_resources
        this._resources.sort((resource1, resource2) => {
            const outsideResourceIndex1 = outsideResources.findIndex(({ id }) => id === resource1.id);
            const outsideResourceIndex2 = outsideResources.findIndex(({ id }) => id === resource2.id);

            return outsideResourceIndex1 - outsideResourceIndex2;
        })

    }

    _reAppend() {
        // Сброс стилей, повторный append
        this._resources.forEach(resource => {
            const { listItemComponent } = resource;
            this._DOM.append(listItemComponent.DOM);
            gsap.set(listItemComponent.DOM, { clearProps: "all" });
        })
    }

    _onDragAndDropSort(listItemComponents) {
        // Мутация, изменения главного массива. Это делается, для того, что бы не тригерить _animateSortBy
        // _animateSortBy - анимирует сортировку, когда обновления приходят из-вне
        this._resources = listItemComponents.map(listItemComponent => this._resources.find(resource => resource.listItemComponent === listItemComponent));
        this._reAppend();
        storage.set(this.storageKEY, this.clone(this._resources));
    }

    _translateItem(listItemComponent, number) {
        return gsap.to(listItemComponent.DOM, { yPercent: 100 * number }).then();
    }

    addListener(type, callback) {
        this._listeners[type] = callback;
    }

    onBeforeRemove(resource) {
        this._listeners["beforeRemove"]?.(resource);
    }
    onAfterRemove() {}
    onAfterRemoveAnimation() {}
    onAfterAdd() {}
    onAfterAddAnimation() {}


    /**
     * Создает массив новых resource ячеек, которые нужно будет запушить в общий массив this._resources
     * @param {{id: String, name: String}[]} outsideResources - массив ресурсов
     * @returns {{id: String, name: String}[]} - массив новых ресурсов
     * @private
     */
    _createNewResourceItems(outsideResources) {
        return outsideResources
            // Фильтруем одинаковые ресурсы
            .reduce((uniqueOutsideResources, outsideResource) => uniqueOutsideResources.map(({ id }) => id).includes(outsideResource.id) ? uniqueOutsideResources : [...uniqueOutsideResources, outsideResource], [])
            // Фильтруем, отбрасываем уже существующие ресурсы
            .filter(outsideResource => !this._resources.map(({ id }) => id).includes(outsideResource.id))
            // Создаем массив из полностью новых resource
            .map(outsideResource => Object.assign({}, outsideResource))
    }

    /**
     * Создает в resource ячейке компонент ListItem, проверяя его наличие
     * @param {Boolean} showInstantly - показать мгновенно (без анимации появления)
     * @returns {ListItem[]} - массив компонентов
     * @private
     */
    _createListItems(showInstantly) {
        return this._resources
            // Фильтруем, пропускаем resource, у которых нет listItemComponent
            .filter(resource => !resource.hasOwnProperty("listItemComponent"))
            // Создаем массив, и записываем в resources создаваемый listItemComponent
            .map(resource => {
                resource.listItemComponent = new this._ListItem({
                    name: resource.name,
                    onClickRemove: () => this._onItemClickRemove(resource),
                    ...this._listItemProps,
                });
                // this._addDragAndDrop(resource.listItemComponent);
                return resource.listItemComponent;
            });
    }

    /**
     * Вставляет DOM компонента в родительский контейнер.
     * Возвращает promise анимации добавления.
     * @param {ListItem} listItemComponent - компонент
     * @param {Boolean} showInstantly - показать мгновенно (без анимации появления)
     * @returns {Promise}
     * @private
     */
    _appendListItems(listItemComponent, showInstantly) {
        // Добавление класса в HTML элемент, вставка в родительский контейнер
        listItemComponent.DOM.classList.add(styles["list__item"]);
        this._DOM.append(listItemComponent.DOM);

        // Анимировать его появление
        if (!showInstantly) {
            return listItemComponent.add();
        }
    }

    /**
     * МУТАЦИЯ this._resources!
     * Удаляет лишние ячейки resources, сравнивая с входящими outsideResources.
     * Возвращает массив listItemComponent, на которых следует вызвать метод remove и destroy
     * @param {{id: String, name: String}[]} outsideResources - массив ресурсов
     * @returns {(ListItem)[]|[]}
     * @private
     */
    _deleteUselessResources(outsideResources) {
        return [...this._resources]
            .filter(resource => {
                if (outsideResources.map(({ id }) => id).includes(resource.id)) return;
                return this._deleteResource(resource);
            })
            .map(resource => {
                // Создаем массив из элементов resource в элементы listItemComponent
                return resource.listItemComponent;
            })
    }

    /**
     * МУТАЦИЯ this._resources!
     * Удаление resource из массива resources.
     * Возвращает listItemComponent или null, если тот отсутствует
     * @param {{id: String, name: String, listItemComponent: ListItem}} resource
     * @returns {ListItem|null}
     * @private
     */
    _deleteResource(resource) {
        this.onBeforeRemove(this.clone(resource));
        this._resources.splice(this._resources.findIndex(r => r === resource), 1);
        return resource.listItemComponent || null;
    }

    /**
     * Запуск анимаций удаления, полное удаление из DOM
     * @param {ListItem[]} listItemComponents
     * @returns {Promise}
     * @private
     */
    _animationRemoving(listItemComponents) {
        return Promise.all(listItemComponents.map(async listItemComponent => {
            // Анимация удаления
            await listItemComponent.remove();

            // Полное удаление из DOM
            listItemComponent.destroy();
        }))
    }

    /**
     * Создает клон без listItemComponent, в resource
     * Это для того, что бы в store не хранить ссылку на этот компонент
     * @param {{id: String, name: String, listItemComponent?: ListItem}|{id: String, name: String, listItemComponent?: ListItem}[]} resources
     * @returns {{id: String, name: String }|{id: String, name: String }[]}
     * @private
     */
    clone(resources) {
        if (Array.isArray(resources)) {
            return resources.map(resource => this._clone(resource))
        } else {
            return this._clone(resources);
        }
    }

    _clone(resource) {
        const instance = {...resource};
        delete instance.listItemComponent;
        return instance;
    }

    /**
     * Описывает события при клике на кнопку элемента "удалить"
     * @param {{id: String, name: String, listItemComponent: ListItem}} resource
     * @returns {Promise<void>}
     * @private
     */
    async _onItemClickRemove(resource) {

        // set массива без ячейки resource. Ячейки без listItemComponent, благодаря команде clone
        const resourcesForOutside = this._resources.filter(({id}) => id !== resource.id).map(resource => this.clone(resource));

        storage.set(this.storageKEY, resourcesForOutside);
    }
}