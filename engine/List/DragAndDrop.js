import { gsap } from "engine";
import * as styles from "./list.module.css";
import {capitalize} from "lodash";

export default class DragAndDropListItems {
    constructor(parent, onComplete) {
        this._listItemComponents = [];
        this._parent = parent;
        this._onComplete = onComplete;

        this._startClientY = null;
        this._targetComponent = null;
        this._targetToInsert = null;
        this._place = null;
        this._self = null;
        this._dragStarted = false;

        this._bind = {
            mousemove: this._mousemove.bind(this),
            mouseup: this._mouseup.bind(this),
            preventDefault: this._preventDefault.bind(this)
        }
    }

    addComponents(listItemComponents) {
        this._listItemComponents = [...this._listItemComponents, ...listItemComponents];

        listItemComponents.forEach(listItemComponent => {
            listItemComponent.DOM.addEventListener("dragstart", event => event.preventDefault());
            listItemComponent._label.addEventListener("mousedown", event => this._mousedown(event, listItemComponent));
        })
    }

    removeComponents(listItemComponents) {
        this._listItemComponents = this._listItemComponents.filter(listItemComponent => !listItemComponents.includes(listItemComponent))
    }

    _mousedown(event, listItemComponent) {
        if (this._dragStarted) return;

        this._startClientY = event.clientY;
        this._targetComponent = listItemComponent;
        this._targetComponent._label.style.cursor = "grabbing";

        window.addEventListener("mousemove", this._bind.mousemove);
        window.addEventListener("mouseup", this._bind.mouseup);
    }

    _mousemove({ clientY }) {
        const deltaY = this._startClientY - clientY;

        if (Math.abs(deltaY) >= 1 && !this._dragStarted) {
            this._dragStarted = true;
            this._targetComponent.DOM.addEventListener("click", this._bind.preventDefault); // для отмены нажатия по checkbox
            gsap.set(this._targetComponent.DOM, { overflow: "visible", zIndex: 10 });
            gsap.to(this._targetComponent.DOM, { height: 0, duration: 0.3 });
            document.body.style.cursor = "grabbing";
        }


        this._targetToInsert?.classList.remove(`${styles["list__item-target-top"]}`, `${styles["list__item-target-bottom"]}`);
        this._targetToInsert = null;
        this._place = null;
        this._self = null;

        if (!this._dragStarted) return;

        gsap.set(this._targetComponent._wrapper, { y: clientY - this._startClientY });

        let last = this._listItemComponents[this._listItemComponents.length - 1].DOM === this._targetComponent.DOM
            ? this._listItemComponents[this._listItemComponents.length - 2]?.DOM
            : this._listItemComponents[this._listItemComponents.length - 1].DOM;

        let first = this._listItemComponents[0].DOM === this._targetComponent.DOM
            ? this._listItemComponents[1]?.DOM
            : this._listItemComponents[0].DOM;

        if (last && clientY >= last.getBoundingClientRect().top + 40) {
            this._targetToInsert = last;
            this._place = "bottom";
        }
        else if (first && clientY < first.getBoundingClientRect().top) {
            this._targetToInsert = first;
            this._place = "top";
        } else {
            const x = this._targetComponent.DOM.getBoundingClientRect().left;
            this._targetComponent.DOM.hidden = true;
            let elemBelow = document.elementFromPoint(x + 150, clientY);
            this._targetComponent.DOM.hidden = false;

            let result;
            let i = 0;
            while(elemBelow && i < 7) {
                i += 1;
                result = this._listItemComponents.find(listItemComponent => listItemComponent.DOM === elemBelow);
                if (result) {
                    break;
                } else {
                    elemBelow = elemBelow.parentElement;
                }
            }
            if (result) {
                this._targetToInsert = result.DOM;
                this._place = clientY > result.DOM.getBoundingClientRect().top + 20 ? "bottom" : "top";
            } else {
                this._targetToInsert = this._targetComponent.DOM;
                this._place = "top";
                this._self = true;
            }
        }

        this._targetToInsert.classList.add(`${this._place === "bottom" ? styles["list__item-target-bottom"] : styles["list__item-target-top"]}`);
    }

    async _mouseup() {
        window.removeEventListener("mousemove", this._bind.mousemove);
        window.removeEventListener("mouseup", this._bind.mouseup);
        this._targetComponent._label.style.cursor = "";
        document.body.style.cursor = "";

        if (!this._dragStarted) return;

        this._targetToInsert.classList.remove(`${styles["list__item-target-top"]}`, `${styles["list__item-target-bottom"]}`);

        let targetToInsertOffset =  this._targetToInsert.getBoundingClientRect().top - this._parent.getBoundingClientRect().top;
        this._place === "bottom" && (targetToInsertOffset += 40);

        await gsap.timeline()
            .set(this._targetComponent._wrapper, { position: "absolute" }, 0)
            .set(this._targetComponent._DOM, { position: "static" }, 0)
            .to(this._targetComponent._wrapper, { y: "", top: targetToInsertOffset, duration: 0.3 }, 0)
            .to(this._targetToInsert, { [`margin${capitalize(this._place)}`]: 40, duration: 0.3 }, 0)
            .set(this._targetComponent._wrapper, { clearProps: "all" })
            .set(this._targetComponent.DOM, { clearProps: "all" })
            .then();

        this._targetComponent.DOM.removeEventListener("click", this._bind.preventDefault);

        if (!this._self) { // todo: если self - то вообще отменять это действие, и не пушить в store ничего, а вернуть как было

            /*
                function arraymove(arr, fromIndex, toIndex) {
                    var element = arr[fromIndex];
                    arr.splice(fromIndex, 1);
                    arr.splice(toIndex, 0, element);
                }
             */

            const targetResource = this._listItemComponents.find((listItemComponent) => listItemComponent.DOM === this._targetComponent.DOM);
            this._listItemComponents = this._listItemComponents.filter((listItemComponent) => listItemComponent.DOM !== this._targetComponent.DOM)
            let insertIndex = this._listItemComponents.findIndex((listItemComponent) => listItemComponent.DOM === this._targetToInsert);
            this._place === "bottom" && (insertIndex += 1);

            this._listItemComponents.splice(insertIndex, 0, targetResource);
            this._onComplete(this._listItemComponents);
        }

        this._dragStarted = false;
    }

    _preventDefault(event) {
        event.preventDefault();
    }
}