export default class Storage {
    constructor() {
        this._listeners = {};
        // this._listenersBeforeUpdate = {};
    }

    set(key, property) {
        this[key] = property;
        this._dispatch(key);
    }

    addListener(key, listener) {
        this._listeners[key] ||= [];
        this._listeners[key].push(listener);
    }

    // addListenerBeforeUpdate(listener) {
    //     this._listenersBeforeUpdate[key] ||= [];
    //     this._listenersBeforeUpdate[key].push(listener);
    // }

    _dispatch(key) {
        this._listeners[key]?.forEach(listener => listener(this[key]))
    }
}