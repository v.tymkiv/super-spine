export default class Resizer {
    constructor(applicationContainer) {
        this._applicationContainer = applicationContainer;
        this._listeners = [];

        window.addEventListener("resize", () => this._onResize());
    }

    addResizeListener(listener) {
        this._listeners.push(listener);
    }

    removeResizeListener(listener) {
        this._listeners = this._listeners.filter(recordedListener => recordedListener !== listener);
    }

    dispatchResize() {
        this._onResize();
    }

    get width() {
        return this._applicationContainer.offsetWidth;
    }

    get height() {
        return this._applicationContainer.offsetHeight;
    }

    _onResize() {
        this._listeners.forEach(listener => listener());
    }
}