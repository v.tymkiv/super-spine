import App from "../components/App";

const appComponent = new App();
const app = appComponent.DOM;

document.getElementById("root").appendChild(app);

appComponent.updateSize();