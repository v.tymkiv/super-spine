import { Component, storage, List } from "engine";
import * as styles from "./layers.module.css";

export default class Layers extends Component {
    constructor(onBtnClick = () => {}) {
        super();

        this._onBtnClick = onBtnClick;

        this._listComponent = new List({
            storageKEY: "layers-list",
            infoItemProps: {text: "There are no any layers"}
        });

        // this._listComponent.addListener("beforeRemove", resource => {
        //     resource.spine.destroy();
        // });

        this._DOM = this._htmlToElement(`<div class="${ styles["layers"] }"></div>`);

        this._header = this._htmlToElement(`<div class="${ styles["layers__header"] }"></div>`);
        this._title = this._htmlToElement(`<h2 class="${ styles["layers__title"] }">Layers</h2>`);
        this._btn = this._htmlToElement(`<button class="${ styles["layers__btn"] } button" type="button">Add layer(s)</button>`);
        this._listWrapper =this._htmlToElement(`<div class="${styles["list-wrapper"]} scrollbar"></div>`);
        this._list = this._listComponent.DOM;

        this._DOM.append(this._header)
        this._header.append(this._title)
        this._header.append(this._btn);
        this._DOM.append(this._listWrapper);
        this._listWrapper.append(this._list);

        this._addListeners();
    }

    _addListeners() {
        this._btn.addEventListener("click", () => this._onBtnClick());
        storage.set("layers-list", []);
    }
}