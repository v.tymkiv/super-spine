import {Component, gsap, storage} from "engine";
import * as styles from "./timeline-list.module.css";
import TimelineListItem from "../TimelineListItem";

export default class TimelineList extends Component {
    constructor({ storageKEY, scroller, leftSide }) {
        super();

        this.storageKEY = storageKEY;
        this._scroller = scroller;
        this._leftSide = leftSide;

        // this.DOMLeft = this._htmlToElement(`<ul class="${styles["list-left"]}"></ul>`);
        this.DOMLeft = this._htmlToElement(`<ul></ul>`);
        this.DOMRight = this._htmlToElement(`<ul></ul>`);
        // this.DOMRight = this._htmlToElement(`<ul class="${styles["list-right"]}"></ul>`);

        this._resources = [];
        storage.addListener(this.storageKEY, (outsideResources) => this._update(outsideResources));
    }

    async _update(outsideResources = []) {
        // Удаление лишних ресурсов. Создание массива listItemComponent, которых нужно будет удалить
        const listItemComponentsToDelete = this._deleteUselessResources(outsideResources);
        this.onAfterRemove();

        // Копирую внутренности outsideResource в this._resource
        // Если этого не сделать, то рассинхронизация может привести к багу
        this._resources = this._resources.map(resource => ({
            ...resource,
            ...outsideResources.find(({id}) => id === resource.id)
        }));

        // Добавление новых resource ячеек
        const newResourceItems = this._createNewResourceItems(outsideResources);
        this._resources.push(...newResourceItems);
        this.onAfterAdd(this.clone(newResourceItems));

        // Все анимация должны быть вызваны только после изменения this_resources
        // Иначе, при одновременном срабатывании сразу нескольких update - будут ошибочно созданы или удалены лишние элементы

        // Анимация удаления
        await this._animationRemoving(listItemComponentsToDelete);
        this.onAfterRemoveAnimation();

        // Создание новых listItemComponent, ожидание окончания анимаций появления
        const listItemComponents = this._createListItems();
        await Promise.all(listItemComponents.map(listItemComponent => this._appendListItems(listItemComponent)));
        this.onAfterAddAnimation();

        if(this._isNeededSorting(outsideResources)) {
            // Анимация сортировки
            await this._animateSortBy(outsideResources);

            this._sortResourcesBy(outsideResources);
        };


    }

    onAfterRemove() {}
    onAfterAdd() {}
    onAfterRemoveAnimation() {};
    onAfterAddAnimation() {};

    _deleteUselessResources(outsideResources) {
        return [...this._resources]
            .filter(resource => {
                if (outsideResources.map(({ id }) => id).includes(resource.id)) return;
                return this._deleteResource(resource);
            })
            .map(resource => {
                // Создаем массив из элементов resource в элементы listItemComponent
                return resource.listItemComponent;
            })
    }

    _deleteResource(resource) {
        this._resources.splice(this._resources.findIndex(r => r === resource), 1);
        return resource.listItemComponent || null;
    }

    _createNewResourceItems(outsideResources) {
        return outsideResources
            // Фильтруем одинаковые ресурсы
            .reduce((uniqueOutsideResources, outsideResource) => uniqueOutsideResources.map(({ id }) => id).includes(outsideResource.id) ? uniqueOutsideResources : [...uniqueOutsideResources, outsideResource], [])
            // Фильтруем, отбрасываем уже существующие ресурсы
            .filter(outsideResource => !this._resources.map(({ id }) => id).includes(outsideResource.id))
            // Создаем массив из полностью новых resource
            .map(outsideResource => Object.assign({}, outsideResource))
    }

    _animationRemoving(listItemComponents) {
        return Promise.all(listItemComponents.map(async listItemComponent => {
            // Анимация удаления
            await listItemComponent.remove();

            // Полное удаление из DOM
            listItemComponent.destroy();
        }))
    }

    _createListItems() {
        return this._resources
            // Фильтруем, пропускаем resource, у которых нет listItemComponent
            .filter(resource => !resource.hasOwnProperty("listItemComponent"))
            // Создаем массив, и записываем в resources создаваемый listItemComponent
            .map(resource => {
                resource.listItemComponent = new TimelineListItem({
                    name: resource.name,
                    animations: resource.animations,
                    thumbs: resource.thumbs ||= [],
                    storageKEY: this.storageKEY,
                    id: resource.id,
                    scroller: this._scroller,
                    leftSide: this._leftSide,
                    onItemUpdate: (id, thumbsForOutside) => this._onItemUpdate(id, thumbsForOutside),
                });
                return resource.listItemComponent;
            });
    }

    _appendListItems(listItemComponent) {
        // Добавление класса в HTML элемент, вставка в родительский контейнер
        // listItemComponent.DOMLeft.classList.add(styles["list__item-left"]);
        // listItemComponent.DOMRight.classList.add(styles["list__item-right"]);
        this.DOMLeft.append(listItemComponent.DOMLeft);
        this.DOMRight.append(listItemComponent.DOMRight);

        // Анимировать его появление
        return listItemComponent.add();
    }

    _animateSortBy(outsideResources) {
        return Promise.all(
            outsideResources
                .reduce(({top = 0, translateInfo = []}, outsideResource) => {
                    const component = this._resources.find(({id}) => id === outsideResource.id).listItemComponent;

                    const element = component.DOMRight;

                    return{
                        translateInfo: [...translateInfo, {translateY : top - element.offsetTop, component}],
                        top: top + element.offsetHeight
                    };
                }, {})
                .translateInfo
                .map(({translateY, component}) => {
                    return gsap.timeline()
                        .to(component.DOMLeft, { y: translateY, duration: 0.3 }, 0)
                        .to(component.DOMRight, { y: translateY, duration: 0.3 }, 0)
                        .then();
                })
        );
    }

    _isNeededSorting(outsideResources) {
        return this._resources.reduce((isNeed, resource, index) => resource.id === outsideResources[index].id ? isNeed : true , false);
    }

    _sortResourcesBy(outsideResources) {
        // Сортировка внутри this_resources
        this._resources.sort((resource1, resource2) => {
            const outsideResourceIndex1 = outsideResources.findIndex(({ id }) => id === resource1.id);
            const outsideResourceIndex2 = outsideResources.findIndex(({ id }) => id === resource2.id);

            return outsideResourceIndex1 - outsideResourceIndex2;
        })

        // Сброс стилей, повторный append
        this._resources.forEach(resource => {
            const { listItemComponent } = resource;
            this.DOMLeft.append(listItemComponent.DOMLeft);
            this.DOMRight.append(listItemComponent.DOMRight);
            gsap.set(listItemComponent.DOMLeft, { clearProps: "y" });
            gsap.set(listItemComponent.DOMRight, { clearProps: "y" });
        })
    }

    _onItemUpdate(id, thumbsForOutside) {
        const index = this._resources.findIndex(resource => resource.id === id);
        const resourcesForOutside = this.clone(this._resources);
        resourcesForOutside[index].thumbs = thumbsForOutside;
        storage.set(this.storageKEY, resourcesForOutside);
    }

    clone(resources) {
        if (Array.isArray(resources)) {
            return resources.map(resource => this._clone(resource))
        } else {
            return this._clone(resources);
        }
    }

    _clone(resource) {
        const instance = {...resource};
        delete instance.listItemComponent;
        return instance;
    }


}