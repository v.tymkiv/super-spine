import { Component } from "engine";
import * as styles from "./bottom-side.module.css";
import Timeline from "../Timeline";

export default class BottomSide extends Component {
    constructor({onPlayBtnClick, onStopBtnClick, onResetBtnClick}) {
        super();

        this._timelineComponent = new Timeline({onPlayBtnClick, onStopBtnClick, onResetBtnClick});

        this._DOM = this._htmlToElement(`<div class="${styles["bottom-side"]}"></div>`);
        this._wrapper = this._htmlToElement(`<div class="${styles["bottom-side__wrapper"]}"></div>`);
        this._timeline = this._timelineComponent.DOM;

        this._DOM.append(this._wrapper)
        this._wrapper.append(this._timeline)
    }
}