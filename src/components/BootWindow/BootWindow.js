import { Component, storage, gsap, PIXI } from "engine";
import Darken from "../Darken";
import AddLayersBtn from "../AddLayersBtn";
import BootSide from "../BootSide";

import * as styles from "./bootWindow.module.css";
import BootListItem from "../BootListItem";
import BootList from "../BootList";

export default class BootWindow extends Component {
    constructor() {
        super();

        /* Components */

        this._darkenComponent = new Darken();
        this._addLayersBtnComponent = new AddLayersBtn();
        this._bootListComponent = new BootList({
            ListItem: BootListItem,
            storageKEY: "boot-window-2",
            listItemProps: {checkedInstantly: true, checkedItemsNumberStorageKEY: "boot-list-number-of-checked-items"},
            infoItemProps: {text: "There are no any booted resources"}
        });
        this._bootSideComponent = new BootSide();

        /* Components */



        /* Elements */

        this._DOM = this._htmlToElement(`<div class="${ styles.bootWindow }"></div>`);
        this._wrapper = this._htmlToElement(`<div class="${ styles.wrapper }"></div>`);
        this._darken = this._darkenComponent.DOM;
        this._darken.classList.add(styles.darken);
        this._mainContainer = this._htmlToElement(`<div class="${ styles.mainContainer }"></div>`);
        this._closeBtn = this._htmlToElement(`<button type="button" class="${ styles.closeBtn } window-close-btn"></button>`);
        this._addLayersBtn = this._addLayersBtnComponent.DOM;
        this._bootList = this._bootListComponent.DOM;

        const topPanel = this._htmlToElement(`<div class="${ styles.topPanel } window-top-panel"></div>`);
        const flexContainer = this._htmlToElement(`<div class="${styles.flexContainer}"></div>`);
        const resourceSide = this._htmlToElement(`<div class="${styles.resourceSide}"></div>`);
        const bootSide = this._bootSideComponent.DOM;
        bootSide.classList.add(styles["boot-side"]);
        const resourceHeader = this._htmlToElement(`<div class="${styles.resourceHeader}"> <h2 class="${styles.resourceHeaderTitle}">Booted resources</h2> </div>`);
        const bootListWrapper = this._htmlToElement(`<div class="${styles.bootListWrapper} scrollbar"></div>`);

        /* Elements */



        /* Внутренне представление

        this._DOM
        ---this._wrapper
        --- ---this._darken
        --- ---this._mainContainer
        --- --- ---this._topPanel
        --- --- --- ---this._closeBtn
        --- --- ---flexContainer
        --- --- --- ---resourceSide
        --- --- --- --- ---resourceHeader
        --- --- --- --- --- ---this._addLayersBtn
        --- --- --- --- ---bootListWrapper
        --- --- --- --- --- ---this._bootList
        --- --- --- ---bootSide


         */



        this._wrapper.append(this._darken, this._mainContainer);
        this._mainContainer.append(topPanel);
        topPanel.append(this._closeBtn);
        this._mainContainer.append(flexContainer);
        flexContainer.append(resourceSide);
        flexContainer.append(bootSide);
        resourceSide.append(resourceHeader);
        resourceSide.append(bootListWrapper);
        resourceHeader.append(this._addLayersBtn);
        bootListWrapper.append(this._bootList);

        gsap.set(this._mainContainer, { y: "100vh", yPercent: -50 });

        this._addListeners();
    }

    open() {
        // Вставляем в ДОМ обертку со всеми элементами компонента
        this._DOM.appendChild(this._wrapper);

        // Деактивировать все _bootListComponent, только после вставки в DOM!
        this._bootListComponent.deactivateAll();

        // Показываем darken
        this._darkenComponent.show();

        this._tween?.kill();
        this._tween = gsap.to(this._mainContainer, { y: 0, x: 0, yPercent: -50, xPercent: -50, duration: 0.3 });
    }

    async close() {
        this._tween?.kill();
        this._tween = gsap.to(this._mainContainer, { y: "100vh", x: 0, yPercent: -50, xPercent: -50, duration: 0.3 });

        // Ждем, пока спрячется mainContainer и пропадет darken.
        await Promise.all([this._tween.then(), this._darkenComponent.hide()]);

        // Удаляем обертки со всеми элементами компонента из DOM
        this._wrapper.remove();
    }

    _addListeners() {
        [this._darken, this._closeBtn].forEach(element => element.addEventListener("click", () => this.close()));

        this._addLayersBtn.addEventListener("click", () => {
            const newResources = this._bootListComponent.getAllActivatedResources().map(resource => {
                const instance = {
                    ...resource,
                    id: `${resource.name}${new Date().getTime()}`,
                    spine: new PIXI.spine.Spine(resource.spineData)
                };
                delete instance.spineData;
                return instance;
            });
            storage.set("layers-list", [...storage["layers-list"], ...newResources]);
            this._bootListComponent.deactivateAll();
        })

        storage.addListener("boot-list-number-of-checked-items", number => this._onCheckedItemsNumberChange(number));
        storage.set("boot-list-number-of-checked-items", 0);
        storage.set("boot-window-2", []);
    }

    _onCheckedItemsNumberChange(number) {
        if (number === 0) {
            this._addLayersBtnComponent.deactivate();
        } else {
            this._addLayersBtnComponent.activate();
        }
    }
}