import {ListItem} from "engine";
import * as styles from "./boot-list-item.module.css";

export default class BootListItem extends ListItem {
    constructor(props) {
        super(props);

        this._checkedItemsNumberStorageKEY = props.checkedItemsNumberStorageKEY;

        this._checkbox.addEventListener("change", () => this._checkboxChange());
    }

    _createDOM({ checkedInstantly, checkedItemsNumberStorageKEY }) {
        super._createDOM();

        storage.set(checkedItemsNumberStorageKEY, storage[checkedItemsNumberStorageKEY] + (checkedInstantly ? 1 : 0)); // todo: *
        this._checkbox = this._htmlToElement(`<input type="checkbox" ${checkedInstantly && "checked"} class="${ styles["item__checkbox"] }" />`); // todo *
        this._label.prepend(this._checkbox);
        this._title.classList.add(styles["item__title"])
    }

    remove() {
        this.checked = false;
        return super.remove();
    }

    get checked() {
        return this._checkbox.checked;
    }

    set checked(checked) {
        if (this._checkbox.checked === checked) return;

        this._checkbox.click();
    }

    _checkboxChange() {
        storage.set(this._checkedItemsNumberStorageKEY, storage[this._checkedItemsNumberStorageKEY] + (this.checked ? 1 : -1));
    }
}