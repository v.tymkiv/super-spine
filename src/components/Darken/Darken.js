import { Component, gsap } from "engine";
import * as styles from "./darken.module.css";

export default class Darken extends Component {
    constructor() {
        super();

        this._DOM = this._htmlToElement(`<div class="${styles.darken}"></div>`);

        gsap.set(this._DOM, { opacity: 0 });
    }

    show() {
        this._tween?.kill();
        this._tween = gsap.to(this._DOM, { opacity: 1, duration: 0.3 });
    }

    hide() {
        this._tween?.kill();
        this._tween = gsap.to(this._DOM, { opacity: 0, duration: 0.3 });
        return this._tween.then()
    }
}