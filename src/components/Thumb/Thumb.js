import {Component, gsap} from "engine";
import * as styles from "./thumb.module.css";
import DragManager from "./DragManager";

export default class Thumb extends Component {
    constructor({ animations = [], scroller, leftSide, animationName, onClickRemove = () => {}, onAnimationChange = () => {} }) {
        super();

        this._onClickRemove = onClickRemove;
        this._onAnimationChange = onAnimationChange;

        this._scroller = scroller;

        this._animations = animations;

        this.timeStart = 0;

        this._DOM = this._htmlToElement(`<div class="${styles["thumb"]}"></div>`);
        this._wrapper = this._htmlToElement(`<div class="${styles["thumb__wrapper"]}"></div>`);

        this._item = this._htmlToElement(`<div class="${styles["thumb__item"]}"></div>`);

        this._duration = this._htmlToElement(`<div class="${styles["thumb__duration"]}"></div>`);

        this._removeBtn = this._htmlToElement(`<button type="button" class="${styles["thumb_remove-btn"]}"></button>`);

        let selectTemplate = `<select class="${styles["item__select"]}">`;
        this._animations.forEach(animation => {
            selectTemplate += `<option ${animationName === animation.name && "selected"} value="${animation.name}">${animation.name}</option>`
        })
        selectTemplate += `</select>`;

        this._select = this._htmlToElement(selectTemplate);

        this._DOM.append(this._wrapper);
        this._wrapper.append(this._item);
        this._wrapper.append(this._removeBtn);
        this._item.append(this._select);
        this._item.append(this._duration);

        gsap.set(this._wrapper, { yPercent: -100 });
        gsap.set(this._DOM, { height: 0 });

        new DragManager({elementOnDrag: this._item, elementToDrag: this._wrapper, parentElement: this._DOM, scroller, leftSide, onDragEnd: x => this._onDragEnd(x)})

        this._addListeners();
    }

    add() {
        this._timeline?.kill();
        this._timeline = gsap.timeline()
            .to(this._wrapper, { yPercent: 0, duration: 0.3 }, 0)
            .to(this._DOM, { height: 90, duration: 0.3 }, 0)
            .to(this._duration, { width: this.animation.duration * 100, duration: 0.3 }, 0);

        return this._timeline.then();
    }

    remove() {

        this._timeline?.kill();
        this._timeline = gsap.timeline()
            .to(this._DOM, { height: 0, duration: 0.3 }, 0)
            .to(this._wrapper, { yPercent: -100, duration: 0.3 }, 0)
            .to(this._wrapper, { x: 0, duration: 0.3 })
            .to(this._scroller, { scrollLeft: 0, duration: 0.3 }, "<")
            .to(this._DOM, { width: "auto", duration: 0.3 }, "<");

        return this._timeline.then();
    }

    set animation(value) {
        if (!value) return;

        this._select.value = value;

        this._tween?.kill();
        this._tween = gsap.to(this._duration, { width: this.animation.duration * 100, duration: 0.3 });
    }

    get animation() {
        return this._animations.find(({name}) => name === this._select.value);
    }

    _onDragEnd(x) {
        this.timeStart = x * 10;
        this._onAnimationChange(this);
    }

    _addListeners() {
        this._removeBtn.addEventListener("click", () => this._onClickRemove(this));
        this._select.addEventListener("change", () => this._onAnimationChange(this));
        this._select.addEventListener("mousedown", (e) => {e.stopPropagation()});
    }
}