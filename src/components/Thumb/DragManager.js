import { gsap } from "engine";
import * as styles from "./thumb.module.css";

export default class DragManager {
    constructor({ elementOnDrag, elementToDrag, parentElement, scroller, leftSide, onDragEnd = () => {} }) {

        this._elementOnDrag = elementOnDrag;
        this._elementToDrag = elementToDrag;
        this._parentElement = parentElement;
        this._scroller      = scroller;
        this._leftSide      = leftSide;
        this._onDragEnd     = onDragEnd;

        this._maxSpeed = 5; // максимальная скорость расширения и скролла, при перетаскивании thumb за границы
        this._tensionRange = 15; // ширина "натяжения"
        this._deltaX = 0;
        this._startX = 0;
        this._x = 0;
        this._lastX = 0;
        this._rightBorder = 0;
        this._leftBorder = 0;

        this._bind = {
            mousedown: this._mousedown.bind(this),
            tick: this._tick.bind(this),
            mousemove: this._mousemove.bind(this),
            mouseup: this._mouseup.bind(this)
        }

        this._elementOnDrag.addEventListener("dragstart", e => e.preventDefault());
        this._elementOnDrag.addEventListener("mousedown", this._bind.mousedown);

        // scroller.addEventListener("scroll", () => {
        //     this._rightBorder = this._getUpdatedRightBorder();
        //     this._leftBorder = this._getUpdatedLeftBorder();
        //     this._rightBorder = Math.max(this._rightBorder, this._lastX);
        //     this._leftBorder = Math.min(this._leftBorder, this._lastX);
        // })
    }

    _mousedown(e) {
        this._startX = e.clientX;
        this._rightBorder = this._getUpdatedRightBorder();
        this._leftBorder = this._getUpdatedLeftBorder();
        this._rightBorder = Math.max(this._rightBorder, this._lastX);
        this._leftBorder = Math.min(this._leftBorder, this._lastX);

        window.addEventListener("mousemove", this._bind.mousemove);
        window.addEventListener("mouseup", this._bind.mouseup);
        gsap.ticker.add(this._bind.tick);

        document.body.classList.add("grabbing");
        this._elementToDrag.classList.add(styles["grabbing"]);
    }

    _tick() {
        this._x = this._deltaX + this._lastX;

        const borderEnd = this._parentElement.offsetWidth - this._elementToDrag.offsetWidth;

        const rightBorderMinThreshold = this._getUpdatedRightBorder();
        const leftBorderMinThreshold = this._getUpdatedLeftBorder();

        this._x = Math.min(this._rightBorder + this._tensionRange, this._x);
        this._x = Math.max(this._leftBorder - this._tensionRange, this._x);

        if (this._x > this._rightBorder) {
            const speed = Math.floor((this._x - this._rightBorder) * this._maxSpeed / this._tensionRange);
            this._x += speed;
            this._rightBorder += speed;
            this._x > borderEnd && gsap.set(this._parentElement, { width: `+=${speed}` });
            this._scroller.scrollLeft += speed;
            gsap.set(this._leftSide, { x: `+=${speed}` }); // А это костыль, мда... но без него дергается левая часть (там где название спайна)
            this._leftBorder = this._getUpdatedLeftBorder();
        }

        if (this._x < this._leftBorder) {
            const speed = Math.floor((this._x - this._leftBorder) * this._maxSpeed / this._tensionRange);
            this._x += speed;
            this._leftBorder += speed;
            this._scroller.scrollLeft += speed;
            gsap.set(this._leftSide, { x: Math.max(gsap.getProperty(this._leftSide, "x") + speed, 0) }); // А это костыль, мда... но без него дергается левая часть (там где название спайна)
            this._rightBorder = this._getUpdatedRightBorder();
        }

        this._rightBorder = Math.max(this._rightBorder, rightBorderMinThreshold);
        this._leftBorder = Math.min(this._leftBorder, leftBorderMinThreshold);
        this._x = Math.max(this._x, 0);

        gsap.set(this._elementToDrag, { x: this._x });
        this._lastX = this._x - this._deltaX;
    }

    _mousemove(e) {
        e.preventDefault();
        this._deltaX = e.clientX - this._startX;
    }

    _mouseup() {
        if (this._x - this._rightBorder <= this._tensionRange && this._x - this._rightBorder > 0) {
            this._x -= Math.floor(this._x - this._rightBorder);
            gsap.to(this._elementToDrag, { x: this._x, duration : 0.3 })
        }

        if (this._leftBorder - this._x  <= this._tensionRange && this._leftBorder - this._x > 0) {
            this._x += Math.floor(this._leftBorder - this._x);
            gsap.to(this._elementToDrag, { x: this._x, duration : 0.3 })
        }

        this._lastX = this._x;
        this._deltaX = 0;
        this._x = 0;

        window.removeEventListener("mousemove", this._bind.mousemove);
        window.removeEventListener("mouseup", this._bind.mouseup);
        gsap.ticker.remove(this._bind.tick);

        document.body.classList.remove("grabbing");
        this._elementToDrag.classList.remove(styles["grabbing"]);

        this._onDragEnd(this._lastX);
    }

    _getUpdatedRightBorder() {
        return this._scroller.scrollLeft + this._scroller.offsetWidth - this._leftSide.offsetWidth - this._elementToDrag.offsetWidth;
    }

    _getUpdatedLeftBorder() {
        return this._scroller.scrollLeft;
    }
}