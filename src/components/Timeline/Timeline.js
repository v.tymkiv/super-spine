import {Component, gsap, storage} from "engine";
import * as styles from "./timeline.module.css";
import TimelineList from "../TimelineList";

export default class Timeline extends Component {
    constructor({onPlayBtnClick, onStopBtnClick, onResetBtnClick}) {
        super();

        this._onPlayBtnClick = onPlayBtnClick;
        this._onStopBtnClick = onStopBtnClick;
        this._onResetBtnClick = onResetBtnClick;

        this._DOM = this._htmlToElement(`<div class="${styles["timeline"]}"></div>`);
        this._playPauseWrapper = this._htmlToElement(`<div class="${styles["timeline__play-pause-wrapper"]}"></div>`);
        this._titleWrapper = this._htmlToElement(`<div class="${styles["timeline__title-wrapper"]}"></div>`);
        this._wrapper = this._htmlToElement(`<div class="${styles["timeline__wrapper"]} scrollbar"></div>`);

        this._playBtn = this._htmlToElement(`<button class="${styles["play-btn"]} button" type="button">Play</button>`);
        this._stopBtn = this._htmlToElement(`<button class="${styles["stop-btn"]} button" type="button">Stop</button>`);
        this._resetBtn = this._htmlToElement(`<button class="button" type="button">Reset</button>`);
        // this._resetBtn = this._htmlToElement(`<button class="${styles["reset-btn"]} button" type="button">Reset</button>`);

        this._leftSide = this._htmlToElement(`<div class="${styles["timeline__left-side"]}"></div>`);
        this._rightSide = this._htmlToElement(`<div class="${styles["timeline__right-side"]}"></div>`);

        this._leftTitle = this._htmlToElement(`<h2 class="${styles["timeline__left-title"]}">Spines</h2>`);
        this._rightTitle = this._htmlToElement(`<h2 class="${styles["timeline__right-title"]}">Timeline</h2>`);

        this._timelineListComponent = new TimelineList({storageKEY: "timeline", scroller: this._wrapper, leftSide: this._leftSide});

        this._leftList = this._timelineListComponent.DOMLeft;
        this._leftList.classList.add(styles["timeline__left-list"]);

        this._rightList = this._timelineListComponent.DOMRight;
        this._rightList.classList.add(styles["timeline__right-list"]);

        this._DOM.append(this._playPauseWrapper);
        this._DOM.append(this._titleWrapper);
        this._DOM.append(this._wrapper);

        this._playPauseWrapper.append(this._playBtn);
        this._playPauseWrapper.append(this._stopBtn);
        this._playPauseWrapper.append(this._resetBtn);

        this._titleWrapper.append(this._leftTitle);
        this._titleWrapper.append(this._rightTitle);

        this._wrapper.append(this._leftSide);
        this._wrapper.append(this._rightSide);

        this._leftSide.append(this._leftList);
        this._rightSide.append(this._rightList);

        this._addListeners();
    }

    _addListeners() {
        storage.set("timeline", []);

        this._playBtn.addEventListener("click", () => this._onPlayBtnClick());
        this._stopBtn.addEventListener("click", () => this._onStopBtnClick());
        this._resetBtn.addEventListener("click", () => this._onResetBtnClick());

        gsap.ticker.add(() => {
            // const top = this._wrapper.scrollTop;
            const left = this._wrapper.scrollLeft;
            gsap.set(this._leftSide, { x: left });
        });

        storage.addListener("layers-list", layersResources => this._updateTimelineStoreByLayersStore(layersResources));
    }

    // Синхронизация между сторами
    _updateTimelineStoreByLayersStore(layersResources) {
        const timelineResources = layersResources.map((layersResource) => {
            const resource = {
                ...(storage["timeline"].find(resource => resource.id === layersResource.id) || {}),
                ...layersResource,
                animations: layersResource.spine.spineData.animations,
            }

            // delete resource.spine;

            return resource;
        });

        storage.set("timeline", timelineResources);
    }
}