import { Component, PIXI, storage, gsap } from "engine";
import * as styles from "./scene.module.css";

export default class Scene extends Component {
    constructor() {
        super();

        this._DOM = this._htmlToElement(`<div class="${styles["scene"]}"></div>`);

        this.app = new PIXI.Application();

        this._DOM.append(this.app.view);

        this._PIXIContainer = new PIXI.Container();

        this.app.stage.addChild(this._PIXIContainer);

        this._resources = [];

        this.app.ticker.stop();

        gsap.ticker.add(() => {
            this.app.ticker.update();
        })

        window.container = this._PIXIContainer;

        window.addEventListener("resize", () => this.updateSize());
        storage.addListener("timeline", (outsideResources) => this._update(outsideResources));
    }

    _update(outsideResources = []) {
        const spinesToRemove = this._deleteUselessResources(outsideResources);

        // Копирую внутренности outsideResource в this._resource
        // Если этого не сделать, то рассинхронизация может привести к багу
        this._resources = this._resources.map(resource => ({
            ...resource,
            ...outsideResources.find(({id}) => id === resource.id)
        }));

        // Добавление новых resource ячеек
        const newResourceItems = this._createNewResourceItems(outsideResources);
        this._resources.push(...newResourceItems);
        newResourceItems.forEach(resource => {
            this._PIXIContainer.addChild(resource.spine);
            resource.spine.visible = false;
        });

        // Удаление спанов со сцены
        spinesToRemove.forEach(spine => {
            spine.destroy();
        })

        // if (this._isNeededSorting(outsideResources)) {
            this._resources.sort((resource1, resource2) => {
                const outsideResourceIndex1 = outsideResources.findIndex(({ id }) => id === resource1.id);
                const outsideResourceIndex2 = outsideResources.findIndex(({ id }) => id === resource2.id);

                return outsideResourceIndex1 - outsideResourceIndex2;
            })


            //Перевтсавка в нужном порядке
            if (this._resources.length) {
                for (let i = this._resources.length - 1; i >= 0; i--) {
                    this._PIXIContainer.addChild(this._resources[i].spine);

                }
            }
        // }


        // this._resources.forEach(({spine}) => {
        //
        // })
    }

    _isNeededSorting(outsideResources) {
        return this._resources.reduce((isNeed, resource, index) => resource.id === outsideResources[index].id ? isNeed : true , false);
    }

    _deleteUselessResources(outsideResources) {
        return [...this._resources]
            .filter(resource => {
                if (outsideResources.map(({ id }) => id).includes(resource.id)) return;
                return this._deleteResource(resource);
            })
            .map(resource => {
                // Создаем массив из элементов resource в элементы listItemComponent
                return resource.spine;
            })
    }

    _deleteResource(resource) {
        this._resources.splice(this._resources.findIndex(r => r === resource), 1);
        return resource.spine || null;
    }

    _createNewResourceItems(outsideResources) {
        return outsideResources
            // Фильтруем одинаковые ресурсы
            .reduce((uniqueOutsideResources, outsideResource) => uniqueOutsideResources.map(({ id }) => id).includes(outsideResource.id) ? uniqueOutsideResources : [...uniqueOutsideResources, outsideResource], [])
            // Фильтруем, отбрасываем уже существующие ресурсы
            .filter(outsideResource => !this._resources.map(({ id }) => id).includes(outsideResource.id))
            // Создаем массив из полностью новых resource
            .map(outsideResource => Object.assign({}, outsideResource))
    }

    updateSize() {
        this.app.renderer.resize(this._DOM.offsetWidth, this._DOM.offsetHeight);
        this._PIXIContainer.position.set(this._DOM.offsetWidth / 2, this._DOM.offsetHeight / 2)
    }
}