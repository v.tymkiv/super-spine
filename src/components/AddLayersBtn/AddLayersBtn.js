import { Component } from "engine";
import * as styles from "./add-layers-btn.module.css";

export default class AddLayersBtn extends Component {
    constructor() {
        super();

        this._DOM = this._htmlToElement(`<button type="button" class="button">Add layers(s)</button>`);
        // this._DOM = this._htmlToElement(`<button type="button" class="${ styles["add-layers-btn"] } button">Add layers(s)</button>`);
    }

    activate() {
        this._DOM.disabled = false;
    }

    deactivate() {
        this._DOM.disabled = true;
    }
}