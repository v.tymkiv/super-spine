import {Component, gsap, storage} from "engine";
import * as styles from "./timeline-list-item.module.css";
import Thumb from "../Thumb";

export default class TimelineListItem extends Component {
    constructor({ name, animations = [], thumbs = [], storageKEY, id, onItemUpdate = () => {}, scroller, leftSide }) {
        super();

        this.name = name;
        this.animations = animations;
        this.id = id;

        this._scroller = scroller;
        this._leftSide = leftSide;

        this.storageKEY = storageKEY;

        this._onItemUpdate = onItemUpdate;

        this._createDOM();

        this._resources = []; // { id, name, timeStart, timeDuration }

        gsap.set(this._wrapperLeft, { yPercent: -100 });
        gsap.set(this._wrapperRight, { yPercent: -100 });
        gsap.set(this.DOMLeft, { height: 0 });
        gsap.set(this.DOMRight, { height: 0 });



        this._update(thumbs);

        this._addListeners();
    }

    _createDOM() {
        this.DOMLeft = this._htmlToElement(`<li class="${styles["item-left"]}"></li>`);
        this.DOMRight = this._htmlToElement(`<li class="${styles["item-right"]}"></li>`);

        this._wrapperLeft = this._htmlToElement(`<div class="${styles["item-left__wrapper"]}"></div>`);
        this._wrapperRight = this._htmlToElement(`<div></div>`);
        // this._wrapperRight = this._htmlToElement(`<div class="${styles["item-right__wrapper"]}"></div>`);

        this._btnWrapper = this._htmlToElement(`<div class="${styles["item__btn-wrapper"]}"></div>`);

        this._title = this._htmlToElement(`<h2 class="${styles["item__title"]}">${ this.name }</h2>`);
        this._btnAdd = this._htmlToElement(`<button class="button" type="button">Add animation</button>`)
        // this._btnAdd = this._htmlToElement(`<button class="${styles["item-btn-add"]} button" type="button">Add animation</button>`)

        this.DOMLeft.append(this._wrapperLeft);
        this.DOMRight.append(this._wrapperRight);

        this._wrapperLeft.append(this._title);
        this._wrapperRight.append(this._btnWrapper);
        this._btnWrapper.append(this._btnAdd);




    }

    async _update(outsideResources) {
        // Удаление лишних ресурсов. Создание массива listItemComponent, которых нужно будет удалить
        const thumbComponentsToDelete = this._deleteUselessResources(outsideResources);

        // Копирую внутренности outsideResource в this._resource
        // Если этого не сделать, то рассинхронизация может привести к багу
        this._resources = this._resources.map(resource => ({
            ...resource,
            ...outsideResources.find(({id}) => id === resource.id)
        }));

        // Присваивание animationName для thumbComponent, под капотом, это обычное присвоение value для select
        // Это присвоение не тригерит событие change, что в свою очередь не вызывает метод set для storage
        // !!Важно!! что бы это не вызывало метод set в storage, потому что это приведет к бесконечному циклу
        this._resources.forEach(({thumbComponent, animationName}) => {
            thumbComponent.animation = animationName;
        })

        // Добавление новых resource ячеек
        const newResourceItems = this._createNewResourceItems(outsideResources);
        this._resources.push(...newResourceItems);

        // Анимация удаления
        this._setHeightForSides(this._resources.length);
        await this._animationRemoving(thumbComponentsToDelete);

        // Создание новых listItemComponent, ожидание окончания анимаций появления
        const thumbComponents = this._createThumbComponents();
        await Promise.all(thumbComponents.map(thumbComponent => this._appendThumbComponents(thumbComponent)));
    }

    _setHeightForSides(number) {
        return gsap.timeline()
            .to(this.DOMLeft, { height: 90 + number * 90, duration: 0.3 }, 0)
            .to(this.DOMRight, { height: 90 + number * 90, duration: 0.3 }, 0)
            .then();
    }

    _deleteUselessResources(outsideResources) {
        return [...this._resources]
            .filter(resource => {
                if (outsideResources.map(({ id }) => id).includes(resource.id)) return;
                return this._deleteResource(resource);
            })
            .map(resource => {
                // Создаем массив из элементов resource в элементы listItemComponent
                return resource.thumbComponent;
            })
    }

    _deleteResource(resource) {
        this._resources.splice(this._resources.findIndex(r => r === resource), 1);
        return resource.thumbComponent || null;
    }

    _createNewResourceItems(outsideResources) {
        return outsideResources
            // Отбрасываем ресурсы, у которых нет id, animations
            // .filter()
            // Фильтруем одинаковые ресурсы
            .reduce((uniqueOutsideResources, outsideResource) => uniqueOutsideResources.map(({ id }) => id).includes(outsideResource.id) ? uniqueOutsideResources : [...uniqueOutsideResources, outsideResource], [])
            // Фильтруем, отбрасываем уже существующие ресурсы
            .filter(outsideResource => !this._resources.map(({ id }) => id).includes(outsideResource.id))
            // Создаем массив из полностью новых resource
            .map(outsideResource => Object.assign({}, outsideResource))
    }

    _animationRemoving(thumbComponents) {
        return Promise.all(thumbComponents.map(async thumbComponent => {
            // Анимация удаления
            await thumbComponent.remove();

            // Полное удаление из DOM
            thumbComponent.destroy();
        }))
    }

    _createThumbComponents() {
        return this._resources
            // Фильтруем, пропускаем resource, у которых нет thumbComponent
            .filter(resource => !resource.hasOwnProperty("thumbComponent"))
            // Создаем массив, и записываем в resources создаваемый thumbComponent
            .map(resource => {
                resource.thumbComponent = new Thumb({
                    id: resource.id,
                    animations: this.animations,
                    scroller: this._scroller,
                    leftSide: this._leftSide,
                    animationName: resource.animationName ||= this.animations[0].name,
                    onClickRemove: (thumbComponent) => this._onThumbClickRemove(thumbComponent),
                    onAnimationChange: (thumbComponent) => this._onAnimationChange(thumbComponent)
                });

                return resource.thumbComponent;
            });
    }

    _appendThumbComponents(thumbComponent) {
        // Добавление класса в HTML элемент, вставка в родительский контейнер
        // thumbComponent.DOM.classList.add(styles["thumb"]);
        this._btnWrapper.before(thumbComponent.DOM);

        // Анимировать его появление
        return thumbComponent.add();
    }

    add() {
        this._timeline?.kill();
        this._timeline = gsap.timeline()
            .to(this._wrapperLeft, { yPercent: 0, duration: 0.3 }, 0)
            .to(this._wrapperRight, { yPercent: 0, duration: 0.3 }, 0)
            .to(this.DOMLeft, { height: 90 + this._resources.length * 90, duration: 0.3 }, 0)
            .to(this.DOMRight, { height: 90 + + this._resources.length * 90, duration: 0.3 }, 0);

        return this._timeline.then();
    }

    remove() {
        this._timeline?.kill();
        this._timeline = gsap.timeline()
            .to(this._wrapperLeft, { yPercent: -100, opacity: 0, duration: 0.3 }, 0)
            .to(this._wrapperRight, { yPercent: -100, duration: 0.3 }, 0)
            .to(this.DOMLeft, { height: 0, duration: 0.3 }, 0)
            .to(this.DOMRight, { height: 0, duration: 0.3 }, 0);

        return this._timeline.then();
    }

    _onThumbClickRemove(thumbComponent) {
        const resourcesForOutside = this.clone(this._resources.filter(resource => resource.thumbComponent !== thumbComponent));
        this._onItemUpdate(this.id, resourcesForOutside);
    }

    _onAnimationChange(thumbComponent) {
        const resourcesForOutside = this.clone(this._resources.map(resource => {
            if (resource.thumbComponent === thumbComponent) {
                resource.animationName = thumbComponent.animation.name;
                resource.timeStart = thumbComponent.timeStart;
            }

            return resource;
        }));

        this._onItemUpdate(this.id, resourcesForOutside);
    }

    _onItemAddClick() {
        const resourcesForOutside = this.clone(this._resources);
        resourcesForOutside.push({ id: `thumb-${new Date().getTime()}`, animationName: this.animations[0].name, timeStart: 0 });
        this._onItemUpdate(this.id, resourcesForOutside);
    }

    _addListeners() {
        storage.addListener(this.storageKEY, (outsideListResources => {
            const resource = outsideListResources.find(({id}) => this.id === id);
            resource?.thumbs && this._update([...resource?.thumbs]);
        }))

        this._btnAdd.addEventListener("click", () => this._onItemAddClick(this));
    }

    clone(resources) {
        if (Array.isArray(resources)) {
            return resources.map(resource => this._clone(resource))
        } else {
            return this._clone(resources);
        }
    }

    _clone(resource) {
        const instance = {...resource};
        delete instance.thumbComponent;
        return instance;
    }

    destroy() {
        this.DOMLeft.remove();
        this.DOMLeft = null;

        this.DOMRight.remove();
        this.DOMRight = null;
    }
}