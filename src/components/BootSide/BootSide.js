import { Component, storage, PIXI } from "engine";
import * as styles from "./boot-side.module.css";

export default class BootSide extends Component {
    constructor() {
        super();

        this._DOM = this._htmlToElement(`<div class="${ styles["boot-side"] }"></div>`);

        this._bootSpace = this._htmlToElement(`<div class="${ styles["boot-side__boot-space"] }"></div>`);
        this._title = this._htmlToElement(`<h2 class="${ styles["boot-side__title"] }">Drop your files here</h2>`);
        this._label = this._htmlToElement(`<label class="${ styles["boot-side__label"] } button text-overflow">Select files</label>`);
        this._input = this._htmlToElement(`<input class="${ styles["boot-side__input"] }" type="file" multiple/>`);

        this._DOM.append(this._bootSpace);
        this._bootSpace.append(this._title);
        this._bootSpace.append(this._label);
        this._label.append(this._input);

        this._addListeners();
    }

    _addListeners() {
        ["dragenter", "dragover", "dragleave", "drop"].forEach(eventName => {
            this._bootSpace.addEventListener(eventName, e => { e.preventDefault(); e.stopPropagation(); }, false);
        });

        ["dragenter", "dragover"].forEach(eventName => {
            this._bootSpace.addEventListener(eventName, () => this._bootSpace.classList.add(styles["highlight"]), false);
        });

        ["dragleave", "drop"].forEach(eventName => {
            this._bootSpace.addEventListener(eventName, () => this._bootSpace.classList.remove(styles["highlight"]), false);
        });

        this._bootSpace.addEventListener("drop", e => this.handleFiles(e.dataTransfer.files), false);
        this._input.addEventListener("change", e => this.handleFiles(e.target.files));
    }

    handleFiles(files) {
        const jsonExist = [...files].some(file => file.name.includes(".json"));
        const pngExist = [...files].some(file => file.name.includes(".png"));
        const atlasExist = [...files].some(file => file.name.includes(".atlas"));

        if (!pngExist) {
            alert("Предупреждение об отсутствии png");
            this._resetInput();
            return;
        }

        if (pngExist && ((!jsonExist && atlasExist) || (jsonExist && !atlasExist))) {
            alert("Предупреждение об отсутствии atlas или json");
            this._resetInput();
            return;
        }

        if(storage["boot-window-2"].some(resource => [...files].some(file => file.name === resource.name))) {
            alert("Предупреждение, такой ресурс уже существует");
            this._resetInput();
            return;
        }

        if (pngExist && !jsonExist && !atlasExist) {
            alert("Вы хотите загрузить спрайт. SuperSpine пока не готов для работы со спрайтами. Все разработчики SuperSpine (Владимир Тымкив tymkiv.vr@gmail.com) работают над этим.");
            this._resetInput();
            return;
        }

        if (pngExist && jsonExist && atlasExist) {
            this._handleSpine(files);
        }
    }

    async _handleSpine(files) {
        const resultList = {
            json: {},
            atlas: {},
            png: [],
            error: null
        };

        await Promise.all([...files].map(async file => {
            const reader = new FileReader();
            const fileType = file.name.includes(".json") ? "json" : file.name.includes(".atlas") ? "atlas" : file.name.includes(".png") ? "png" : "error";
            let result;

            switch (fileType) {
                case "json":
                case "atlas":
                    reader.readAsText(file);
                    break;
                case "png":
                    reader.readAsDataURL(file);
                    break;
                default:
                    resultList.error = true;
                    return;
            }

            try {
                 const event = await new Promise((resolve, reject) => {
                    reader.addEventListener("load", resolve);
                    reader.addEventListener("error", reject);
                });

                 result = event.target.result;
            }
            catch {
                resultList.error = true;
                return;
            }

            Array.isArray(resultList[fileType]) ? resultList[fileType].push({result, name: file.name}) : resultList[fileType] = {result, name: file.name};
        }));

        if(resultList.error) {
            alert("Произошла какая-то ошибка...");
            this._resetInput();
            return;
        }

        const rawSkeletonData = JSON.parse(resultList.json.result);
        const rawAtlasData = resultList.atlas.result;
        const spineAtlas = new PIXI.spine.core.TextureAtlas(rawAtlasData, (line, callback) => {
            const img = new Image();
            img.src = resultList.png.find(data => data.name === line).result;
            PIXI.utils.BaseTextureCache[line] = new PIXI.BaseTexture(img); // Добавляем картинку в хеш
            callback(PIXI.BaseTexture.fromImage(line));
        });

        const spineAtlasLoader = new PIXI.spine.core.AtlasAttachmentLoader(spineAtlas);
        const spineJsonParser = new PIXI.spine.core.SkeletonJson(spineAtlasLoader);

        const spineData = spineJsonParser.readSkeletonData(rawSkeletonData);

        const resource = {
            spineData,
            name: resultList.json.name,
            id: resultList.json.name
        }

        storage.set("boot-window-2", [...storage["boot-window-2"], resource]);

        this._resetInput();
    }

    _resetInput() {
        this._input.files = null;
        this._input.value = "";
    }
}