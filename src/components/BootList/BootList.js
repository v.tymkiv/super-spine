import {List} from "engine";

export default class BootList extends List {
    deactivateAll() {
        this._resources.forEach(({listItemComponent}) => listItemComponent.checked = false);
    }

    getAllActivatedResources() {
        return this._resources
            .filter(resource => resource.listItemComponent.checked)
            .map(resource => {
            return this.clone(resource)
        });
    }
}