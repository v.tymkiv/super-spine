import { Component } from "engine";
import * as styles from "./left-side.module.css";
import Layers from "../Layers";

export default class LeftSide extends Component {
    constructor(onLayersBtnClick) {
        super();

        this._DOM = this._htmlToElement(`<div class="${ styles["left-side"] }"></div>`);

        this._layersComponent = new Layers(onLayersBtnClick);

        this._wrapper = this._htmlToElement(`<div class="${ styles["left-side__wrapper"] }"></div>`);


        this._layers = this._layersComponent.DOM;

        this._DOM.append(this._wrapper);
        this._wrapper.append(this._layers);
    }
}