import { Component, storage, gsap } from "engine";
import * as styles from "./app.module.css";

import LeftSide from "../LeftSide";
import BootWindow from "../BootWindow";
import Scene from "../Scene";
import BottomSide from "../BottomSide";

export default class App extends Component {
    constructor() {
        super();

        this._DOM = this._htmlToElement(`<div class=" ${ styles["app"] } "></div>`);

        this._leftSideComponent = new LeftSide(() => this._openBootWindow());
        this._bootWindowComponent = new BootWindow();
        this._sceneComponent = new Scene();
        this._bottomSideComponent = new BottomSide({onPlayBtnClick: () => this.play(), onStopBtnClick: () => this.stop(), onResetBtnClick: () => this.reset()});

        this._leftSide = this._leftSideComponent.DOM;
        this._bootWindow = this._bootWindowComponent.DOM;
        this._scene = this._sceneComponent.DOM;
        this._bottomSide = this._bottomSideComponent.DOM;

        this._DOM.append(this._leftSide);
        this._DOM.append(this._bootWindow);
        this._DOM.append(this._scene);
        this._DOM.append(this._bottomSide);


        window.play = () => this.play();

        storage.addListener("timeline", () => {
            this.reset()
        });
    }

    async play() {
        if (this._timeline) {
            this._timeline.play();
            storage["timeline"].forEach(timelineItem => {
                if (timelineItem.thumbs?.length) { // Если анимации добавлены
                    timelineItem.spine.state.timeScale = 1;
                    timelineItem.spine.visible = true;
                }

            })
            return;
        }

        this._timeline = gsap.timeline();
        this._promises = [];
        storage["timeline"].forEach(timelineItem => {
            timelineItem.thumbs?.forEach(({animationName, timeStart}) => {
                let resolver;
                const promise = new Promise(resolve => {
                    resolver = resolve;
                    this._timeline.add(() => {
                        timelineItem.spine.visible = true;
                        timelineItem.spine.state.timeScale = 1;
                        timelineItem.spine.state.setAnimation(0, animationName);
                        timelineItem.spine.state.addListener({complete: resolve});
                    }, timeStart / 1000);
                })

                promise.resolve = resolver;

                this._promises.push(promise);
            })
        });
        await Promise.all(this._promises);

        this._timeline = null;
        // this._timeline.play();
    }

    stop() {
        this._timeline?.pause();
        storage["timeline"].forEach(timelineItem => {
            timelineItem.spine.state.timeScale = 0;
        })
    }

    reset() {
        this._timeline?.kill();
        this._promises?.forEach(promise => promise.resolve());

        storage["timeline"].forEach(timelineItem => {
            timelineItem.spine.state.timeScale = 0;
            timelineItem.spine.visible = false;
        })
    }

    updateSize() {
        this._sceneComponent.updateSize();
    }

    _openBootWindow() {
        this._bootWindowComponent.open();
    }
}